FROM gitlab-registry.cern.ch/swan/docker-images/systemuser:v5.9.0

# RUN conda install -y -c conda-forge python-gfal2 \
#     && conda clean --all -f -y

RUN pip install rucio-jupyterlab \
    && pip install --ignore-requires-python rucio-clients~=1.25.7 \
    && jupyter serverextension enable --py rucio_jupyterlab.server --sys-prefix \
    && jupyter lab build \
    && ln -s /usr/local/lib/python3.7/site-packages/rucio_jupyterlab /usr/local/lib/swan/extensions/

ADD systemuser.sh /srv/singleuser/systemuser.sh
